import threading, pynput, time, json

with open('options.json') as file:
    options = json.load(file)

mouse = pynput.mouse.Controller()

pressed = 0

def CPStoSPC(cps):
	# and vice versa
	if cps > 0:
		return 1 / cps
	else:
		return 0
	
def clicking():
	global isLeftPressed
	global isRightPressed
	while True:
		while pressed != 0:
			if pressed == 1:
				mouse.press(pynput.mouse.Button.left)
			elif pressed == 2:
				mouse.press(pynput.mouse.Button.right)
			time.sleep(CPStoSPC(options["cps"]))
			if pressed == 1:
				mouse.release(pynput.mouse.Button.left)
			elif pressed == 2:
				mouse.release(pynput.mouse.Button.right)

clickingThread = threading.Thread(target = clicking, daemon = True)
clickingThread.start()

def onPress(key):
	global pressed
	if str(key) == "'z'":
		pressed = 1
	elif str(key) == "'x'":
		pressed = 2

def onRelease(key):
	global pressed
	if str(key) == "'z'" or str(key) == "'x'":
		pressed = 0

def kbListener():
	with pynput.keyboard.Listener(on_press = onPress, on_release = onRelease) as listener:
		listener.join()

kbListenerThread = threading.Thread(target = kbListener, daemon = True)
kbListenerThread.start()

while True:
	print("OpenClicker v1.0 (C) Grzesiek11 2019\nLicensed under GNU GPL v3.\n")
	cps = options['cps']
	spc = CPStoSPC(cps)
	if cps == 0:
		cps = 'infinity'
		spc = cps
	print(f"CPS: {cps}")
	print(f"SPC: {spc}\n")
	print("[1] Set CPS")
	print("[2] Set SCP")
	print("[3] Map keys")
	print("[0] Exit")
	choice = int(input("> "))
	if choice == 1:
		options["cps"] = float(input("CPS: "))
	elif choice == 2:
		options["cps"] = CPStoSPC(float(input("SPC: ")))
	elif choice == 3:
		print("Not implemented yet.")
	elif choice == 0:
		exit()
	else:
		print("Bad choice!")
